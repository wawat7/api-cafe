-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE menus (
    id          serial NOT NULL PRIMARY KEY,
    name        varchar(255) NOT NULL,
    description text,
    image       varchar(255),
    price       int DEFAULT 0,
    created_at  TIMESTAMP NULL DEFAULT NULL,
    updated_at  TIMESTAMP NULL DEFAULT NULL
);


-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE menus;