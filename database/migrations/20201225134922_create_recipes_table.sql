-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE recipes (
    id              serial NOT NULL PRIMARY KEY,
    menu_id         bigint NOT NULL,
    ingredient_id   bigint NOT NULL,
    created_at      TIMESTAMP NULL DEFAULT NULL,
    updated_at      TIMESTAMP NULL DEFAULT NULL,

    FOREIGN KEY (menu_id) REFERENCES menus(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (ingredient_id) REFERENCES ingredients(id) ON UPDATE CASCADE ON DELETE CASCADE
);


-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE recipes;