-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE transactions (
    id                  serial NOT NULL PRIMARY KEY,
    transaction_code    varchar(255) NOT NULL,
    customer_name       varchar(255) NOT NULL,
    total_price         int DEFAULT 0,
    transaction_type    varchar(255) NOT NULL,
    user_id             bigint NOT NULL,
    created_at          TIMESTAMP NULL DEFAULT NULL,
    updated_at          TIMESTAMP NULL DEFAULT NULL,

    FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE
);


-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE transactions;