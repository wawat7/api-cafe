-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE transaction_details (
    id              serial NOT NULL PRIMARY KEY,
    transaction_id  bigint NOT NULL,
    menu_id         bigint NOT NULL,
    qty             int DEFAULT 0,
    size            char DEFAULT 'S',
    price_detail    int DEFAULT 0,
    created_at      TIMESTAMP NULL DEFAULT NULL,
    updated_at      TIMESTAMP NULL DEFAULT NULL,

    FOREIGN KEY (menu_id) REFERENCES menus(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (transaction_id) REFERENCES transactions(id) ON UPDATE CASCADE ON DELETE CASCADE
);


-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE transaction_details;