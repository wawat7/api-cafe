package handler

import (
	"BE-POS-Coffee/helper"
	"BE-POS-Coffee/ingredient"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type ingredientHandler struct {
	service ingredient.Service
}

func NewIngredientHandler(service ingredient.Service) *ingredientHandler {
	return &ingredientHandler{service}
}

func (h *ingredientHandler) CreateIngredient(c *gin.Context) {
	var input ingredient.CreateIngredientInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed create ingredient", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest,response)
		return
	}
	ingredientData, err := h.service.CreateIngredient(input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed create ingredient", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity,response)
		return
	}
	response := helper.ApiResponse("Create ingredient successfully", http.StatusOK, "success", ingredient.FormatIngredient(ingredientData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *ingredientHandler) GetAll(c *gin.Context) {
	ingredientData, err := h.service.GetAll()
	if err != nil {
		response := helper.ApiResponse("Failed get ingredients", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	fmt.Println(ingredientData)

	response := helper.ApiResponse("List of ingredients", http.StatusOK, "success", ingredient.FormatIngredients(ingredientData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *ingredientHandler) GetByID(c *gin.Context) {
	var input ingredient.GetIngredientInput
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed get ingredient", http.StatusBadRequest, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}
	ingredientData, err := h.service.GetByID(input)
	if err != nil {
		response := helper.ApiResponse("Failed get ingredient", http.StatusBadRequest, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Ingredient detail", http.StatusOK, "success", ingredient.FormatIngredient(ingredientData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *ingredientHandler) UpdateIngredient(c *gin.Context) {
	var inputID ingredient.GetIngredientInput
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		response := helper.ApiResponse("Failed update ingredient", http.StatusBadRequest, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	var inputData ingredient.CreateIngredientInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		response := helper.ApiResponse("Failed update ingredient", http.StatusBadRequest, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	newIngredient, err := h.service.UpdateIngredient(inputID, inputData)
	if err != nil {
		response := helper.ApiResponse("Failed update ingredient", http.StatusBadRequest, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("ingredient updated successfully", http.StatusOK, "success", ingredient.FormatIngredient(newIngredient))
	c.JSON(http.StatusOK, response)
	return
}

func (h *ingredientHandler) RemoveIngredient(c *gin.Context) {
	var input ingredient.GetIngredientInput
	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helper.ApiResponse("Failed delete ingredient", http.StatusBadRequest, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	_, err = h.service.RemoveIngredient(input)
	if err != nil {
		response := helper.ApiResponse("Failed delete ingredient", http.StatusBadRequest, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Deleted ingredient successfully", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
	return
}
