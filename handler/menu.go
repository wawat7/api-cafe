package handler

import (
	"BE-POS-Coffee/helper"
	"BE-POS-Coffee/menu"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

type menuHandler struct {
	service menu.Service
}

func NewMenuHandler(service menu.Service) *menuHandler {
	return &menuHandler{service}
}

func (h *menuHandler) CreateMenu(c *gin.Context) {
	var input menu.CreateMenuInput
	err := c.ShouldBind(&input)
	if err != nil {
		errMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed create menu", http.StatusBadRequest, "error", errMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	//get image
	realPath, err := moveImage(c)
	if err != nil {
		response := helper.ApiResponse("Failed update menu", http.StatusBadRequest, "error", err.Error())
		c.SecureJSON(http.StatusBadRequest, response)
		return
	}
	if realPath != "" {
		input.Image = realPath
	}

	menuData := menu.Menu{
		Name:        input.Name,
		Description: input.Description,
		Image:       realPath,
		Price:       input.Price,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	menuData, err = h.service.CreateMenu(menuData)
	if err != nil {
		errMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed create menu", http.StatusUnprocessableEntity, "error", errMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Create menu successfully", http.StatusOK, "success", menu.FormatMenu(menuData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *menuHandler) GetAll(c *gin.Context) {
	menus, err := h.service.GetAll()
	if err != nil {
		response := helper.ApiResponse("Failed get menus", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("List of menus", http.StatusOK, "success", menu.FormatMenus(menus))
	c.JSON(http.StatusOK, response)
	return
}

func (h *menuHandler) GetByID(c *gin.Context) {
	var input menu.GetMenuDetailInput
	err := c.ShouldBindUri(&input)
	if err != nil {
		errorMessage := gin.H{"error":err.Error()}
		response := helper.ApiResponse("Failed get menu", http.StatusBadRequest, "errpr", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	menuData, err := h.service.GetByID(input.ID)
	if err != nil {
		response := helper.ApiResponse("Failed get menu", http.StatusUnprocessableEntity, "errpr", err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("menu detail", http.StatusOK, "success", menu.FormatMenu(menuData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *menuHandler) UpdateMenu(c *gin.Context) {
	var input menu.CreateMenuInput
	err := c.ShouldBind(&input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed update menu", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	var inputID menu.GetMenuDetailInput
	err = c.ShouldBindUri(&inputID)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed update menu", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	input.ID = inputID.ID

	realPath, err := moveImage(c)
	if err != nil {
		response := helper.ApiResponse("Failed update menu", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	if realPath != "" {
		input.Image = realPath
	}

	menuData, err := h.service.UpdateMenu(input)
	if err != nil {
		response := helper.ApiResponse("Failed update menu", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Update menu succesfully", http.StatusOK, "success", menu.FormatMenu(menuData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *menuHandler) RemoveMenu(c *gin.Context) {
	var inputID menu.GetMenuDetailInput
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed update menu", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	_, err = h.service.RemoveMenu(inputID)
	if err != nil {
		response := helper.ApiResponse("Failed update menu", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Deleted menu has been success", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
	return
}

func moveImage(c *gin.Context) (string, error) {
	realPath := ""
	//get image
	file, _ := c.FormFile("image")
	if file != nil {
		//create folder
		stringPath := "images/menu/"
		err := os.MkdirAll(stringPath, os.ModePerm)
		if err != nil {
			return realPath, err
		}

		//move to directory
		path := fmt.Sprintf("images/menu/%s", file.Filename)
		err = c.SaveUploadedFile(file, path)
		if err != nil {
			return realPath, err
		}

		//Rename file
		random := fmt.Sprintf("images/menu/%d", time.Now().UnixNano() / int64(time.Millisecond))
		realPath = random + filepath.Ext(path)
		fmt.Println(realPath)
		err = os.Rename(path, realPath)
		if err != nil {
			return realPath, err
		}
		return realPath, nil
	}
	return realPath, nil
}
