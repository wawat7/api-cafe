package handler

import (
	"BE-POS-Coffee/helper"
	"BE-POS-Coffee/recipe"
	"github.com/gin-gonic/gin"
	"net/http"
)

type recipeHandler struct {
	service recipe.Service
}

func NewRecipeHandler(service recipe.Service) *recipeHandler {
	return &recipeHandler{service}
}

func (h *recipeHandler) CreateRecipe(c *gin.Context) {
	var input recipe.CreateRecipeInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed create recipe", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = h.service.CreateRecipe(input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed create recipe", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Create recipe successfully", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
	return
}

func (h *recipeHandler) UpdateRecipe(c *gin.Context) {
	var input recipe.CreateRecipeInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed create recipe", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = h.service.UpdateRecipe(input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed create recipe", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Update recipe successfully", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
	return
}
