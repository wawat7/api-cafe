package handler

import (
	"BE-POS-Coffee/auth"
	"BE-POS-Coffee/helper"
	"BE-POS-Coffee/user"
	"github.com/gin-gonic/gin"
	"net/http"
)

type userHandler struct {
	userService user.Service
	authService auth.Service
}

func NewUserHandler(userService user.Service, authService auth.Service) *userHandler {
	return &userHandler{userService, authService}
}

func (h *userHandler) CreateUser(c *gin.Context) {
	var input user.CreateUserInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errorMessage := gin.H{"errors": err.Error()}
		response := helper.ApiResponse("Create user failed", http.StatusBadRequest,"error", errorMessage)
		c.JSON(http.StatusBadRequest,response)
		return
	}

	userData, err := h.userService.CreateUser(input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Create user failed", http.StatusUnprocessableEntity,"error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity,response)
		return
	}
	response := helper.ApiResponse("Create user successfully", http.StatusOK, "success", user.FormatUser(userData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *userHandler) GetAll(c *gin.Context){
	users, err := h.userService.GetAll()
	if err != nil {
		response := helper.ApiResponse("Failed get all user", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("List of users", http.StatusOK, "success", user.FormatUsers(users))
	c.JSON(http.StatusOK, response)
	return
}

func (h *userHandler) GetByID(c *gin.Context) {
	var input user.GetUserDetail
	err := c.ShouldBindUri(&input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed get user detail", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	userData, err := h.userService.GetByID(input.ID)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed get user detail", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("User Detail", http.StatusOK, "success", user.FormatUser(userData))
	c.JSON(http.StatusOK, response)
	return
}

func (h *userHandler) UpdateUser(c *gin.Context) {
	var inputID user.GetUserDetail
	err := c.ShouldBindUri(&inputID)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed to update user", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	var inputData user.UpdateUserInput
	err = c.ShouldBindJSON(&inputData)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed to update user", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	newUser, err := h.userService.UpdateUser(inputID, inputData)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed to update user", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("Update user successfully", http.StatusOK, "success", user.FormatUser(newUser))
	c.JSON(http.StatusOK, response)
	return
}

func (h *userHandler) DeleteUser(c *gin.Context) {
	var input user.GetUserDetail
	err := c.ShouldBindUri(&input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed to delete user", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	_ , err = h.userService.DeleteUser(input.ID)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed to delete user", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}
	response := helper.ApiResponse("User has been deleted", http.StatusOK, "success", nil)
	c.JSON(http.StatusOK, response)
	return
}

func (h *userHandler) Login(c *gin.Context) {
	var input user.LoginUserInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed login", http.StatusBadRequest, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	loggedIn, err := h.userService.Login(input)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed login", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	token, err := h.authService.GenerateToken(loggedIn.ID)
	if err != nil {
		errorMessage := gin.H{"error": err.Error()}
		response := helper.ApiResponse("Failed login token", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}
	response := helper.ApiResponse("Success login", http.StatusOK, "success", user.FormatUserLogin(loggedIn, token))
	c.JSON(http.StatusOK, response)
	return
}