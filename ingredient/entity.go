package ingredient

import (
	"time"
)

type Ingredient struct {
	ID int
	Name string
	Description string
	Stock int
	Type string
	CreatedAt time.Time
	UpdatedAt time.Time
}
