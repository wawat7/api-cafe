package ingredient

type IngredientFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Description string `json:"description"`
	Stock int `json:"stock"`
	Type string `json:"type"`
}

func FormatIngredient(ingredient Ingredient) IngredientFormat {
	return IngredientFormat{
		ID:          ingredient.ID,
		Name:        ingredient.Name,
		Description: ingredient.Description,
		Stock:       ingredient.Stock,
		Type:        ingredient.Type,
	}
}

func FormatIngredients(ingredients []Ingredient) []IngredientFormat {
	var formatters []IngredientFormat

	if len(ingredients) > 0 {
		for _, ingredient := range ingredients {
			formatter := FormatIngredient(ingredient)
			formatters = append(formatters, formatter)
		}
	}
	return formatters
}
