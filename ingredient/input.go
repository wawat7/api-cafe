package ingredient


type CreateIngredientInput struct {
	Name string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
	Stock int `json:"stock" binding:"required"`
	Type string `json:"type" binding:"required"`
}

type GetIngredientInput struct {
	ID int `uri:"id" binding:"required"`
}
