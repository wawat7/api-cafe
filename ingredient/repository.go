package ingredient

import "gorm.io/gorm"

type Repository interface {
	Save(ingredient Ingredient) (Ingredient, error)
	FindAll() ([]Ingredient, error)
	FindByID(ID int) (Ingredient, error)
	Update(ingredient Ingredient) (Ingredient,error)
	Delete(ingredient Ingredient) (bool, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB)	*repository  {
	return &repository{db}
}

func (r *repository) Save(ingredient Ingredient) (Ingredient, error){
	err := r.db.Create(&ingredient).Error
	if err != nil {
		return ingredient, err
	}
	return ingredient, nil
}

func (r *repository) FindAll() ([]Ingredient, error){
	var ingredients []Ingredient
	err := r.db.Find(&ingredients).Error
	if err != nil {
		return ingredients, err
	}
	return ingredients, nil
}

func (r *repository) FindByID(ID int) (Ingredient, error) {
	var ingredient Ingredient
	err := r.db.Where("id = ?", ID).Find(&ingredient).Error
	if err != nil {
		return ingredient, err
	}
	return ingredient, nil
}

func (r *repository) Update(ingredient Ingredient) (Ingredient,error){
	err := r.db.Save(ingredient).Error
	if err != nil {
		return ingredient, err
	}
	return ingredient, nil
}

func (r *repository) Delete(ingredient Ingredient) (bool, error){
	err := r.db.Delete(&ingredient).Error
	if err != nil {
		return false, err
	}
	return true, nil
}
