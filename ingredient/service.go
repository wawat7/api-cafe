package ingredient

import (
	"errors"
	"time"
)

type Service interface {
	CreateIngredient(input CreateIngredientInput) (Ingredient, error)
	GetAll() ([]Ingredient, error)
	GetByID(inputID GetIngredientInput) (Ingredient, error)
	UpdateIngredient(inputID GetIngredientInput, inputData CreateIngredientInput) (Ingredient, error)
	RemoveIngredient(input GetIngredientInput) (bool, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) CreateIngredient(input CreateIngredientInput) (Ingredient, error){
	ingredient := Ingredient{
		Name:        input.Name,
		Description: input.Description,
		Stock:       input.Stock,
		Type:        input.Type,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	newIngredient, err := s.repository.Save(ingredient)
	if err != nil {
		return newIngredient, err
	}
	return newIngredient, nil
}

func (s *service) GetAll() ([]Ingredient, error){
	ingredients, err := s.repository.FindAll()
	if err != nil {
		return ingredients, err
	}
	return ingredients, nil
}

func (s *service) GetByID(input GetIngredientInput) (Ingredient, error){
	ingredientData, err := s.repository.FindByID(input.ID)
	if err != nil {
		return ingredientData, err
	}

	if ingredientData.ID == 0 {
		return ingredientData, errors.New("ingredient not found")
	}
	return ingredientData, nil
}

func (s *service) UpdateIngredient(inputID GetIngredientInput, inputData CreateIngredientInput) (Ingredient, error){
	ingredientData, err := s.GetByID(inputID)
	if err != nil {
		return ingredientData, err
	}

	ingredientData.Name = inputData.Name
	ingredientData.Description = inputData.Description
	ingredientData.Stock = inputData.Stock
	ingredientData.Type = inputData.Type

	newIngredient, err := s.repository.Update(ingredientData)
	if err != nil {
		return newIngredient, err
	}
	return newIngredient, nil
}

func (s *service) RemoveIngredient(input GetIngredientInput) (bool, error){
	ingredientData, err := s.GetByID(input)
	if err != nil {
		return false, err
	}
	_, err = s.repository.Delete(ingredientData)
	if err != nil {
		return false, err
	}
	return true, nil
}
