package main

import (
	"BE-POS-Coffee/auth"
	"BE-POS-Coffee/handler"
	"BE-POS-Coffee/helper"
	"BE-POS-Coffee/ingredient"
	"BE-POS-Coffee/menu"
	"BE-POS-Coffee/recipe"
	"BE-POS-Coffee/user"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"net/http"
	"strings"
)

func main() {
	dsn := "host=localhost user=postgres password=root dbname=pos_coffee port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err.Error())
	}

	userRepository := user.NewRepository(db)
	userService := user.NewService(userRepository)
	authService := auth.NewService()
	userHandler := handler.NewUserHandler(userService, authService)

	ingredientRepository := ingredient.NewRepository(db)
	ingredientService := ingredient.NewService(ingredientRepository)
	ingredientHandler := handler.NewIngredientHandler(ingredientService)

	menuRepository := menu.NewRepository(db)
	menuService := menu.NewService(menuRepository)
	menuHandler := handler.NewMenuHandler(menuService)

	recipeRepository := recipe.NewRepository(db)
	recipeService := recipe.NewService(recipeRepository)
	recipeHandler := handler.NewRecipeHandler(recipeService)


	router := gin.Default()
	router.Use(cors.Default())

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, "server running..")
	})

	api := router.Group("api/v1")
	api.GET("/users", authMiddleware(authService, userService, "admin"),userHandler.GetAll)
	api.POST("/users", authMiddleware(authService, userService, "admin"), userHandler.CreateUser)
	api.GET("/users/:id", authMiddleware(authService, userService, "admin"), userHandler.GetByID)
	api.PUT("users/:id", authMiddleware(authService, userService, "admin"), userHandler.UpdateUser)
	api.DELETE("/users/:id", authMiddleware(authService, userService, "admin"), userHandler.DeleteUser)

	api.POST("/ingredients", ingredientHandler.CreateIngredient)
	api.GET("/ingredients", ingredientHandler.GetAll)
	api.GET("/ingredients/:id", ingredientHandler.GetByID)
	api.PUT("/ingredients/:id", ingredientHandler.UpdateIngredient)
	api.DELETE("/ingredients/:id", ingredientHandler.RemoveIngredient)

	api.POST("/menus", menuHandler.CreateMenu)
	api.GET("/menus", menuHandler.GetAll)
	api.GET("/menus/:id", menuHandler.GetByID)
	api.PUT("/menus/:id", menuHandler.UpdateMenu)
	api.DELETE("/menus/:id", menuHandler.RemoveMenu)

	api.POST("/recipes", recipeHandler.CreateRecipe)
	api.PUT("/recipes", recipeHandler.UpdateRecipe)

	api.POST("/session", userHandler.Login)


	router.Run()

}

func authMiddleware(authService auth.Service, userService user.Service, role string) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if !strings.Contains(authHeader, "Bearer") {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error",nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		tokenString := ""
		arrayToken := strings.Split(authHeader, " ")
		if len(arrayToken) > 0 {
			tokenString = arrayToken[1]
		}
		token, err := authService.ValidateToken(tokenString)
		if err != nil {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", err.Error())
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		fmt.Println(claim)
		userID := int(claim["user_id"].(float64))
		user, err := userService.GetByID(userID)
		if err != nil {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", err.Error())
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		if user.Role != role {
			response := helper.ApiResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}
		c.Set("currentUser", user)
	}
}
