package menu

import (
	"BE-POS-Coffee/recipe"
	"time"
)

type Menu struct {
	ID int
	Name string
	Description string
	Image string
	Price int
	CreatedAt time.Time
	UpdatedAt time.Time
	Recipes []recipe.Recipe
}