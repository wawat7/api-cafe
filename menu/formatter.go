package menu

type MenuFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Description string `json:"description"`
	ImageURL string `json:"image_url"`
	Price int `json:"price"`
	Recipe []RecipeFormat `json:"recipe"`
}

type RecipeFormat struct {
	//ID int `json:"id"`
	Ingredient IngredientFormat `json:"ingredient"`
}

type IngredientFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Description string `json:"description"`
	Stock int `json:"stock"`
	Type string `json:"type"`
}

func FormatMenu(menu Menu) MenuFormat {
	menuFormat := MenuFormat{
		ID:          menu.ID,
		Name:        menu.Name,
		Description: menu.Description,
		ImageURL:    menu.Image,
		Price:       menu.Price,
	}

	recipes := []RecipeFormat{}
	if len(menu.Recipes) > 0 {
		for _, recipe := range menu.Recipes {
			formatRecipe := RecipeFormat{
				//ID:         recipe.ID,
				Ingredient: IngredientFormat{
					ID:          recipe.Ingredient.ID,
					Name:        recipe.Ingredient.Name,
					Description: recipe.Ingredient.Description,
					Stock:       recipe.Ingredient.Stock,
					Type:        recipe.Ingredient.Type,
				},
			}
			recipes = append(recipes, formatRecipe)
		}
	}
	menuFormat.Recipe = recipes

	return menuFormat
}

func FormatMenus(menus []Menu) []MenuFormat {
	formatters := []MenuFormat{}
	if len(menus) > 0 {
		for _, menu := range menus {
			formatter := FormatMenu(menu)
			formatters = append(formatters, formatter)
		}
	}
	return formatters
}


