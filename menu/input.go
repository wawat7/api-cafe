package menu

type CreateMenuInput struct {
	ID int
	Name string `form:"name" binding:"required"`
	Description string `form:"description" binding:"required"`
	Image string
	Price int `form:"price" binding:"required"`
}

type GetMenuDetailInput struct {
	ID int `uri:"id" binding:"required"`
}