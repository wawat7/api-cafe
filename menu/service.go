package menu

import (
	"errors"
	"os"
)

type Service interface {
	CreateMenu(menu Menu) (Menu, error)
	GetAll() ([]Menu, error)
	GetByID(ID int) (Menu, error)
	UpdateMenu(input CreateMenuInput) (Menu, error)
	RemoveMenu(inputID GetMenuDetailInput) (bool, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) CreateMenu(menu Menu) (Menu, error){
	newMenu, err := s.repository.Save(menu)
	if err != nil {
		return newMenu, err
	}
	return newMenu, nil
}

func (s *service) GetAll() ([]Menu, error){
	menus, err := s.repository.FindAll()
	if err != nil {
		return menus, err
	}
	return menus, nil
}

func (s *service) GetByID(ID int) (Menu, error){
	menu, err := s.repository.FindByID(ID)
	if err != nil {
		return menu, err
	}

	if menu.ID == 0 {
		return menu, errors.New("menu not found")
	}
	return menu, nil
}

func (s *service) UpdateMenu(input CreateMenuInput) (Menu, error){
	menu, err := s.GetByID(input.ID)
	if err != nil {
		return menu, err
	}

	menu.Name = input.Name
	menu.Description = input.Description
	menu.Price = input.Price

	if input.Image != "" {
		//remove image
		err = removeImage(menu.Image)

		menu.Image = input.Image
	}

	menu, err = s.repository.Update(menu)
	if err != nil {
		return menu, err
	}
	return menu, nil
}

func (s *service) RemoveMenu(input GetMenuDetailInput) (bool, error){
	menu, err := s.GetByID(input.ID)
	if err != nil {
		return false, err
	}

	//remove image
	err = removeImage(menu.Image)
	if err != nil {
		return false, err
	}

	_ , err = s.repository.Delete(menu)
	if err != nil {
		return false, err
	}
	return true, nil

}

func removeImage(path string) error {
	err := os.Remove(path)
	if err != nil {
		return err
	}
	return nil
}
