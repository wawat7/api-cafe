package recipe

import (
	"BE-POS-Coffee/ingredient"
	"time"
)

type Recipe struct {
	ID int
	MenuID int
	IngredientID int
	CreatedAt time.Time
	UpdatedAt time.Time
	Ingredient ingredient.Ingredient
}
