package recipe

type CreateRecipeInput struct {
	MenuID int `json:"menu_id" binding:"required"`
	IngredientID []int `json:"ingredient_id"`
}
