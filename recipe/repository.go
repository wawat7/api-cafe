package recipe

import (
	"gorm.io/gorm"
)

type Repository interface {
	Save(recipes []Recipe) error
	CheckMenuAvailable(MenuID int) (bool, error)
	CheckRecipeAvailable(MenuID int, IngredientID int) (bool, error)
	DeleteByMenuID(MenuID int) error
	FindByMenuID(MenuID int) ([]Recipe, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) Save(recipes []Recipe) error{
	err := r.db.Save(&recipes).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *repository) CheckMenuAvailable(MenuID int) (bool, error){
	var recipe Recipe
	err := r.db.Where("menu_id = ?", MenuID).Find(&recipe).Error
	if err != nil {
		return false, err
	}

	if recipe.ID != 0 {
		return true, nil
	}
	return false, nil
}

func (r *repository) FindByMenuID(MenuID int) ([]Recipe, error){
	var recipes []Recipe
	err := r.db.Where("menu_id = ?", MenuID).Find(&recipes).Error
	if err != nil {
		return recipes, err
	}
	return recipes, nil
}

func (r *repository) CheckRecipeAvailable(MenuID int, IngredientID int) (bool, error){
	var recipe Recipe
	err := r.db.Where("menu_id = ? and ingredient_id = ?", MenuID, IngredientID).Find(&recipe).Error
	if err != nil {
		return false, err
	}
	if recipe.ID != 0 {
		return true, nil
	}
	return false, nil
}

func (r *repository) DeleteByMenuID(MenuID int) error{
	err := r.db.Where("menu_id = ?", MenuID).Delete(&Recipe{}).Error
	if err != nil {
		return err
	}
	return nil
}
