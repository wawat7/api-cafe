package recipe

import (
	"errors"
	"time"
)

type Service interface {
	CreateRecipe(input CreateRecipeInput) error
	UpdateRecipe(input CreateRecipeInput) error
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) CreateRecipe(input CreateRecipeInput) error{
	checkMenuAvailable, err := s.repository.CheckMenuAvailable(input.MenuID)
	if err != nil {
		return err
	}

	if checkMenuAvailable {
		return errors.New("menu has exist, please update if change the recipe")
	}

	if checkMenuAvailable == false && len(input.IngredientID) > 0 {
		var recipes []Recipe
		for _, ingredient := range input.IngredientID {
			recipe := Recipe{
				MenuID:       input.MenuID,
				IngredientID: ingredient,
				CreatedAt:    time.Now(),
				UpdatedAt:    time.Now(),
			}
			recipes = append(recipes, recipe)
		}
		err = s.repository.Save(recipes)
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("ingredient is empty")
}

func (s *service) UpdateRecipe(input CreateRecipeInput) error{
	err := s.repository.DeleteByMenuID(input.MenuID)
	if err != nil {
		return err
	}

	err = s.CreateRecipe(input)
	if err != nil {
		return err
	}
	return nil
}