package user

import "time"

type UserFormatter struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Email string `json:"email"`
	Role string `json:"role"`
	CreatedAt time.Time `json:"created_at"`
}

func FormatUser(user User) UserFormatter {
	return UserFormatter{
		ID: 	   user.ID,
		Name:      user.Name,
		Email:     user.Email,
		Role:      user.Role,
		CreatedAt: user.CreatedAt,
	}
}

func FormatUsers(users []User) []UserFormatter {
	var formatters []UserFormatter
	if len(users) > 0 {
		for _, user := range users {
			formatter := FormatUser(user)
			formatters = append(formatters, formatter)
		}
	}
	return formatters
}

type UserLoginFormat struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Email string `json:"email"`
	Role string `json:"role"`
	Token string `json:"token"`
}

func FormatUserLogin(user User, token string) UserLoginFormat {
	return UserLoginFormat{
		ID:    user.ID,
		Name:  user.Name,
		Email: user.Email,
		Role:  user.Role,
		Token: token,
	}
}
