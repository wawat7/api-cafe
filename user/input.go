package user

type CreateUserInput struct {
	Name string `json:"name" binding:"required"`
	Email string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
	Role string `json:"role"`
}

type GetUserDetail struct {
	ID int `uri:"id" binding:"required"`
}

type UpdateUserInput struct {
	Name string `json:"name" binding:"required"`
	Email string `json:"email" binding:"required,email"`
	Password string `json:"password"`
	Role string `json:"role"`
}

type LoginUserInput struct {
	Email string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}