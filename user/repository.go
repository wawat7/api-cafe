package user

import (
	"errors"
	"gorm.io/gorm"
)

type Repository interface {
	Save(user User) (User, error)
	FindByEmail(email string) (User, error)
	FindAll() ([]User, error)
	FindByID(ID int) (User, error)
	Update(user User) (User, error)
	Delete(user User) (bool,error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) Save(user User) (User, error){

	//validate email
	userData, err := r.FindByEmail(user.Email)
	if err != nil {
		return user, nil
	}
	if userData.ID != 0 {
		return user, errors.New("email already exist")
	}

	err = r.db.Create(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (r *repository) FindByEmail(email string) (User, error){
	var user User
	err := r.db.Where("email = ?",email).Find(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (r *repository) FindAll() ([]User, error){
	var user []User
	err := r.db.Find(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (r *repository) FindByID(ID int) (User, error){
	var user User
	err := r.db.Where("id = ?", ID).Find(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (r *repository) Update(user User) (User, error){
	err := r.db.Save(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (r *repository) Delete(user User) (bool, error){
	err := r.db.Delete(&user).Error
	if err != nil {
		return false, err
	}
	return true, nil
}
