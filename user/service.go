package user

import (
	"errors"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type Service interface {
	CreateUser(input CreateUserInput) (User, error)
	GetAll() ([]User, error)
	GetByID(ID int) (User, error)
	UpdateUser(inputID GetUserDetail, inputData UpdateUserInput) (User, error)
	DeleteUser(ID int) (bool, error)
	Login(input LoginUserInput) (User, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) CreateUser(input CreateUserInput) (User, error){
	user := User{
		Name:      input.Name,
		Email:     input.Email,
		Password:  "",
		Role:      "cashier",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	if input.Role == "admin" {
		user.Role = "admin"
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
	if err != nil {
		return user, err
	}
	user.Password = string(passwordHash)

	newUser, err := s.repository.Save(user)
	if err != nil {
		return newUser, err
	}
	return newUser, nil
}

func (s *service) GetAll() ([]User, error) {
	users, err := s.repository.FindAll()
	if err != nil {
		return users, err
	}

	return users, nil
}

func (s *service) GetByID(ID int) (User, error){
	user, err := s.repository.FindByID(ID)
	if err != nil {
		return user, err
	}

	if user.ID == 0 {
		return user, errors.New("User not found")
	}
	return user, nil
}

func (s *service) UpdateUser(inputID GetUserDetail, inputData UpdateUserInput) (User, error){
	user, err := s.GetByID(inputID.ID)
	if err != nil {
		return user, err
	}

	checkEmail, err := s.repository.FindByEmail(inputData.Email)
	if err != nil {
		return user, err
	}

	if checkEmail.Email != "" && checkEmail.ID != inputID.ID {
		return user, errors.New("email already exist")
	}

	user.Name = inputData.Name
	user.Email = inputData.Email

	if inputData.Password != "" {
		passwordHash, err := bcrypt.GenerateFromPassword([]byte(inputData.Password), bcrypt.MinCost)
		if err != nil {
			return user, err
		}
		user.Password = string(passwordHash)
	}

	if inputData.Role != "" {
		if inputData.Role == "admin" {
			user.Role = "admin"
		}else if inputData.Role == "cashier" {
			user.Role = "cashier"
		}
	}
	user.UpdatedAt = time.Now()

	user, err = s.repository.Update(user)
	if err != nil {
		return user, err
	}
	return user, nil
}

func (s *service) DeleteUser(ID int) (bool, error){
	user, err := s.GetByID(ID)
	if err != nil {
		return false, err
	}

	deleted, err := s.repository.Delete(user)
	if err != nil {
		return false, err
	}
	return deleted, nil
}

func (s *service) Login(input LoginUserInput) (User, error){
	user, err := s.repository.FindByEmail(input.Email)
	if err != nil {
		return user, err
	}

	fmt.Println(user)
	fmt.Println(input)
	if user.ID == 0 {
		return user, errors.New("user not found")
	}

	//password check
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password))
	if err != nil {
		return user, errors.New("Password is wrong")
	}
	return user, nil
}
